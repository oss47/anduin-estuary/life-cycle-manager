use std::vec::Vec;

#[derive(Debug)]
pub struct State {
    name: &'static str,
}

#[derive(Debug)]
pub struct Transition {
    source_state: State,
    destination_state: State,
    actions: Vec<Action>,
    post_actions: Vec<Action>,
}

#[derive(Debug)]
pub struct Action {
    name: &'static str,
    implementation: Box<ActionImpl>
}

trait ActionImpl {
    fn execute(&mut self);
}

#[cfg(test)]
mod test {
    use core::State;

    #[test]
    fn debug_simple_state() {
        let state = State { name: "Test State" };
        println!("{:?}", state);
    }
}